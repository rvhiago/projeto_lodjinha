package com.example.projetolojinha;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.projetolojinha.objects.MaisVendidosObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.projetolojinha.utilitarios.Constants.DATA_OBJECT_VENDIDOS;
import static com.example.projetolojinha.utilitarios.Constants.JSON_RESULT;
import static com.example.projetolojinha.utilitarios.Constants.POST_ERROR_MSG;
import static com.example.projetolojinha.utilitarios.Constants.POST_SUCESS_MSG;
import static com.example.projetolojinha.utilitarios.Constants.ROTA_POST_PRODUTO;
import static com.example.projetolojinha.utilitarios.Constants.URL_PADRAO;

public class DescProdutosFloat extends AppCompatActivity {

    ImageView imgDesc;
    TextView txtTituloDesc, txtPrecoAntesDesc, txtPrecoDepoisDesc, txtSubTituloDesc, txtDescricaoProdDesc, txtTESTE;
    ProgressDialog mProgressDialog;
    private int ID_PRODUTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desc_produtos_float);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgDesc = findViewById(R.id.imgDesc);
        txtTituloDesc = findViewById(R.id.txtTituloDesc);
        txtPrecoAntesDesc = findViewById(R.id.txtPrecoAntesDesc);
        txtPrecoDepoisDesc = findViewById(R.id.txtPrecoDepoisDesc);
        txtSubTituloDesc = findViewById(R.id.txtSubTituloDesc);
        txtDescricaoProdDesc = findViewById(R.id.txtDescricaoProdDesc);


        Bundle extras = getIntent().getExtras();

        if(!extras.isEmpty()){
            MaisVendidosObject dados = (MaisVendidosObject) extras.get(DATA_OBJECT_VENDIDOS);

            assert dados != null;
            RequestOptions options = new RequestOptions()
                    .error(R.drawable.logo)
                    .priority(Priority.HIGH);

            Glide.with(this)
                    .asBitmap()
                    .apply(options)
                    .load(dados.getUrlImagem())

                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@android.support.annotation.Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            //progressPicture.setVisibility(View.GONE);

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                            return false;
                        }
                    })
                    .into(imgDesc);

            txtTituloDesc.setText(dados.getNome());
            txtPrecoAntesDesc.setText(dados.getPrecoDe());
            txtPrecoDepoisDesc.setText(dados.getPrecoPor());
            txtSubTituloDesc.setText("");
            //txtDescricaoProdDesc.setText(dados.getDescricao());
            txtDescricaoProdDesc.setText(Html.fromHtml(dados.getDescricao()));

            getSupportActionBar().setTitle(dados.getNome());

            txtPrecoAntesDesc.setPaintFlags(txtPrecoAntesDesc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            ID_PRODUTO = dados.getId();

        }else{
            Toast.makeText(this, "Não foi possível trazer as informações", Toast.LENGTH_SHORT).show();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                postProduto(ID_PRODUTO);
            }
        });
    }


    public void postProduto(int id){
        showProgressDialog();
        AndroidNetworking.post(URL_PADRAO+ROTA_POST_PRODUTO+id)
                //.setPriority(Priority.NORMAL)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dismissDialog();
                        try {

                            String result = (String) response.get(JSON_RESULT);

                            if(result.equalsIgnoreCase(POST_SUCESS_MSG)){
                                addSucesso();

                            }else if(result.equalsIgnoreCase(POST_ERROR_MSG)){

                                Toast.makeText(DescProdutosFloat.this, "Problemas para adicionar produto!", Toast.LENGTH_SHORT).show();


                            }else{
                                Toast.makeText(DescProdutosFloat.this, "Erro no servidor!", Toast.LENGTH_SHORT).show();

                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        dismissDialog();
                    }
                });
    }


        public void showProgressDialog(){
            mProgressDialog = new ProgressDialog(DescProdutosFloat.this);
            mProgressDialog.setTitle("Aguarde");
            mProgressDialog.setMessage("Adicionando produto...");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
            //mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, textButton, onClickListener);
            mProgressDialog.show();
    }


    public void addSucesso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(DescProdutosFloat.this);
        builder.setMessage("Produto reservado com sucesso!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    public void dismissDialog(){
        mProgressDialog.dismiss();
    }

}
