package com.example.projetolojinha.objects;

public class BannerObject {
    private int id;
    private String linkUrl;
    private String urlImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public BannerObject(int id, String linkUrl, String urlImage) {
        this.id = id;
        this.linkUrl = linkUrl;
        this.urlImage = urlImage;
    }
}
