package com.example.projetolojinha.objects;

import android.graphics.Bitmap;

import com.example.projetolojinha.objects.CategoriasObject;

import java.io.Serializable;

public class MaisVendidosObject implements Serializable {

    int id;
    String nome;
    String urlImagem;
    String descricao;
    String precoDe;
    String precoPor;
    CategoriasObject categoriasObject;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPrecoDe() {
        return precoDe;
    }

    public void setPrecoDe(String precoDe) {
        this.precoDe = precoDe;
    }

    public String getPrecoPor() {
        return precoPor;
    }

    public void setPrecoPor(String precoPor) {
        this.precoPor = precoPor;
    }

    public CategoriasObject getCategoriasObject() {
        return categoriasObject;
    }

    public void setCategoriasObject(CategoriasObject categoriasObject) {
        this.categoriasObject = categoriasObject;
    }

    public MaisVendidosObject(int id, String nome, String urlImagem, String descricao, String precoDe, String precoPor) {
        this.id = id;
        this.nome = nome;
        this.urlImagem = urlImagem;
        this.descricao = descricao;
        this.precoDe = precoDe;
        this.precoPor = precoPor;
    }


    public MaisVendidosObject(int id, String nome, String urlImagem, String descricao, String precoDe, String precoPor, CategoriasObject categoriasObject) {
        this.id = id;
        this.nome = nome;
        this.urlImagem = urlImagem;
        this.descricao = descricao;
        this.precoDe = precoDe;
        this.precoPor = precoPor;
        this.categoriasObject = categoriasObject;
    }

    public MaisVendidosObject() {

    }
}
