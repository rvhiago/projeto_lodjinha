package com.example.projetolojinha.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.projetolojinha.R;
import com.example.projetolojinha.objects.BannerObject;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderAdapter extends PagerAdapter {
    private Context context;
    private List<BannerObject> image;

    public SliderAdapter(Context context, List<BannerObject> image) {
        this.context = context;
        this.image = image;
    }

    @Override
    public int getCount() {
        return image.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pager, null);

        ImageView imageView = view.findViewById(R.id.imageView);

        BannerObject banner = image.get(position);

       /* Uri uri = Uri.parse(image.get(position).getUrlImage());
        imageView.setImageURI(uri);*/

        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.banner1));

        Picasso.get()
                .load(banner.getLinkUrl())
                .error(R.drawable.logo)
                .into(imageView);

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position,@NonNull  Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
