package com.example.projetolojinha.fragment;

/**
 * Created by anonymous on 11/4/16.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.projetolojinha.ProdutoPorCategoria;
import com.example.projetolojinha.R;
import com.example.projetolojinha.objects.CategoriasObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.projetolojinha.utilitarios.Constants.CATEGORIA_DESC;
import static com.example.projetolojinha.utilitarios.Constants.CATEGORIA_ID;
import static com.example.projetolojinha.utilitarios.Constants.CATEGORIA_IMAGE;
import static com.example.projetolojinha.utilitarios.Constants.DATA_OBJECT_VENDIDOS;
import static com.example.projetolojinha.utilitarios.Constants.ROTA_CATEGORIAS;
import static com.example.projetolojinha.utilitarios.Constants.URL_PADRAO;

public class HorizontalListViewFragment extends Fragment {

    List<CategoriasObject> listitems = new ArrayList<>();
    RecyclerView MyRecyclerView;
    ProgressBar progressBanner3;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_horizontal_list_view, container, false);
        MyRecyclerView = view.findViewById(R.id.cardView);
        MyRecyclerView.setHasFixedSize(true);
        progressBanner3 = view.findViewById(R.id.progressBanner3);

        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        String url = URL_PADRAO+ROTA_CATEGORIAS;

        progressBanner3.setVisibility(View.VISIBLE);
        listitems.clear();
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        progressBanner3.setVisibility(View.GONE);
                        try {
                            JSONArray array = (JSONArray) jsonObject.get("data");

                            for(int j=0; j<array.length(); j++){
                                JSONObject json = (JSONObject) array.get(j);

                                CategoriasObject object = new CategoriasObject(
                                        json.getInt(CATEGORIA_ID),
                                        json.getString(CATEGORIA_DESC),
                                        json.getString(CATEGORIA_IMAGE));

                                listitems.add(object);
                            }

                            if (listitems.size() > 0 & MyRecyclerView != null) {
                                MyRecyclerView.setAdapter(new MyAdapter(listitems));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("ON-anError", "============="+ anError.toString());
                        progressBanner3.setVisibility(View.GONE);
                    }
                });

        Log.d("RECYCLER", "SIZE=============="+ listitems.size());
        MyRecyclerView.setLayoutManager(MyLayoutManager);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
        private List<CategoriasObject> list;

        public MyAdapter(List<CategoriasObject> Data) {
            list = Data;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycle_items, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {


            holder.tvname.setText(list.get(position).getNome());
            //holder.iv.(R.drawable.ic_like);
            RequestOptions options = new RequestOptions()
                    .error(R.drawable.logo)
                    .centerInside()
                    .priority(com.bumptech.glide.Priority.HIGH);

            Glide.with(getContext())
                    .asBitmap()
                    .apply(options)
                    .load(list.get(position).getImage())
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@android.support.annotation.Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            //progressPicture.setVisibility(View.GONE);
                            //Toast.makeText(context, "Erro ao carregar foto de perfil", Toast.LENGTH_SHORT).show();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {


                            return false;
                        }
                    })
                    .into(holder.iv);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvname;
        public ImageView iv;

        public MyViewHolder(View v) {
            super(v);

            tvname = (TextView) v.findViewById(R.id.name);
            iv = (ImageView) v.findViewById(R.id.imgView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            CategoriasObject object = listitems.get(getAdapterPosition());
            Intent ir = new Intent(getContext(), ProdutoPorCategoria.class);
            ir.putExtra(DATA_OBJECT_VENDIDOS, object);
            getContext().startActivity(ir);
        }
    }
}