package com.example.projetolojinha;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class InfoDev extends AppCompatActivity {


    TextView txtObs;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_dev);

        getSupportActionBar().setTitle("Obs Dev");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        txtObs = findViewById(R.id.txtObs);

        txtObs.setText("• As imagens que estiverem com o ícone do app, é porque a URL da imagem vinda do servidor não está 'boa'.\n\n" +
                       "• Por questão de tempo, pode ser que algumas especificações de designer não estejam de acordo com o solicitado. \n\n" +
                       "• Algumas categorias(Filmes e Séries, etc) não possuem produtos, por isso, não trará nenhuma lista. \n\n" +
                       "• Por ter sido solicitado apenas dois itens de menu lateral, optei por usar apenas activity ao invés de fragment.\n\n" +
                       "• O código em si não está bem organizado, porém, novamente, so tive 2 dias pra poder fazer o desafio. Então acabei que foquei " +
                "em fazer tudo funcionar.");
    }
}
