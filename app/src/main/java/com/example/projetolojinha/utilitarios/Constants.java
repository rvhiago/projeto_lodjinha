package com.example.projetolojinha.utilitarios;

public abstract class Constants {

    public static String DATA_OBJECT_VENDIDOS = "data";
    public static String URL_PADRAO = "https://alodjinha.herokuapp.com/";

    public static String ROTA_PRODUTO_MAIS_VENDIDO = "produto/maisvendidos";
    public static String ROTA_BANNER = "banner";
    public static String ROTA_CATEGORIAS = "categoria";


    /**
     * Variáveis JSON MAIS VENDIDOS
     */
    public static String VENDIDO_ID = "id";
    public static String VENDIDO_NOME = "nome";
    public static String VENDIDO_IMAGEM = "urlImagem";
    public static String VENDIDO_DESCRICAO = "descricao";
    public static String VENDIDO_PRECO_DE = "precoDe";
    public static String VENDIDO_PRECO_POR = "precoPor";


    /**
     * Variáveis JSON BANNER
     */
    public static String BANNER_ID = "id";
    public static String BANNER_IMAGE = "urlImagem";
    public static String BANNER_LINK = "linkUrl";

    /**
     * Variaveis JSON CATEGORIA
     */
    public static String CATEGORIA_ID  = "id";
    public static String CATEGORIA_DESC = "descricao";
    public static String CATEGORIA_IMAGE  = "urlImagem";
    //=============
    public static String PARAMETRO_CATEGORIA = "categoriaId";

    /**
     * Variaveis para POST
     */

    public static String ROTA_POST_PRODUTO = "produto/";
    public static String ROTA_GET_PRODUTO = "produto/";

    //-----
    /**
     * RESULTS
     */
    public static String POST_SUCESS_MSG = "success";
    public static String POST_ERROR_MSG ="error";
    public static String JSON_RESULT = "result";




}
