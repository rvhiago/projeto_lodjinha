package com.example.projetolojinha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.projetolojinha.adapter.MaisVendidosAdapter;
import com.example.projetolojinha.objects.CategoriasObject;
import com.example.projetolojinha.objects.MaisVendidosObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import static com.example.projetolojinha.utilitarios.Constants.DATA_OBJECT_VENDIDOS;
import static com.example.projetolojinha.utilitarios.Constants.PARAMETRO_CATEGORIA;
import static com.example.projetolojinha.utilitarios.Constants.ROTA_GET_PRODUTO;
import static com.example.projetolojinha.utilitarios.Constants.URL_PADRAO;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_DESCRICAO;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_ID;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_IMAGEM;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_NOME;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_PRECO_DE;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_PRECO_POR;

public class ProdutoPorCategoria extends AppCompatActivity {

    private ListView lvCategorias;
    private List<MaisVendidosObject> objectList;
    private MaisVendidosAdapter adapter;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produto_por_categoria);

        lvCategorias = findViewById(R.id.lvCategorias);
        progress = findViewById(R.id.progress);

        Bundle extras = getIntent().getExtras();

        if(!extras.isEmpty()){
            CategoriasObject dados = (CategoriasObject) extras.get(DATA_OBJECT_VENDIDOS);
            getItensMaisVendidos(dados.getId());

            getSupportActionBar().setTitle(dados.getNome());

        }else{
            Toast.makeText(this, "Não foi possível trazer as informações", Toast.LENGTH_SHORT).show();
        }

        lvCategorias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MaisVendidosObject object = (MaisVendidosObject) parent.getAdapter().getItem(position);
                Intent ir = new Intent(ProdutoPorCategoria.this, DescProdutosFloat.class);
                ir.putExtra(DATA_OBJECT_VENDIDOS, object);
                startActivity(ir);
            }
        });

    }

    public void getItensMaisVendidos(int id){
        objectList = new ArrayList<>();

        progress.setVisibility(View.VISIBLE);
        String url = URL_PADRAO+ROTA_GET_PRODUTO;
        AndroidNetworking.get(url)
                .setPriority(Priority.MEDIUM)
                .addQueryParameter(PARAMETRO_CATEGORIA, String.valueOf(id))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d("ON-RESPONSE", "============="+ jsonObject.toString());

                        progress.setVisibility(View.GONE);
                        try {
                            JSONArray array = (JSONArray) jsonObject.get("data");

                            for(int j=0; j<array.length(); j++){
                                JSONObject json = (JSONObject) array.get(j);

                                MaisVendidosObject object = new MaisVendidosObject(
                                        json.getInt(VENDIDO_ID),
                                        json.getString(VENDIDO_NOME),
                                        json.getString(VENDIDO_IMAGEM),
                                        json.getString(VENDIDO_DESCRICAO),
                                        json.getString(VENDIDO_PRECO_DE),
                                        json.getString(VENDIDO_PRECO_POR));

                                objectList.add(object);
                            }

                            if(array.length() == 0){
                                Toast.makeText(ProdutoPorCategoria.this, "Dados vazios do servidor", Toast.LENGTH_SHORT).show();
                            }
                            adapter = new MaisVendidosAdapter(ProdutoPorCategoria.this, objectList);
                            lvCategorias.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("ON-anError", "============="+ anError.toString());
                        Toast.makeText(ProdutoPorCategoria.this, "Erro ao trazer os dados", Toast.LENGTH_SHORT).show();
                        progress.setVisibility(View.GONE);
                        //progressBanner2.setVisibility(View.GONE);
                    }

                });
    }
}
