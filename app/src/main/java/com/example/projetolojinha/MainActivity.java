package com.example.projetolojinha;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.projetolojinha.adapter.MaisVendidosAdapter;
import com.example.projetolojinha.adapter.SliderAdapter;
import com.example.projetolojinha.fragment.HorizontalListViewFragment;
import com.example.projetolojinha.objects.BannerObject;
import com.example.projetolojinha.objects.MaisVendidosObject;
import com.jacksonandroidnetworking.JacksonParserFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

import static com.example.projetolojinha.utilitarios.Constants.BANNER_ID;
import static com.example.projetolojinha.utilitarios.Constants.BANNER_IMAGE;
import static com.example.projetolojinha.utilitarios.Constants.BANNER_LINK;
import static com.example.projetolojinha.utilitarios.Constants.DATA_OBJECT_VENDIDOS;
import static com.example.projetolojinha.utilitarios.Constants.ROTA_BANNER;
import static com.example.projetolojinha.utilitarios.Constants.ROTA_PRODUTO_MAIS_VENDIDO;
import static com.example.projetolojinha.utilitarios.Constants.URL_PADRAO;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_DESCRICAO;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_ID;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_IMAGEM;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_NOME;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_PRECO_DE;
import static com.example.projetolojinha.utilitarios.Constants.VENDIDO_PRECO_POR;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private Toolbar toolbar;
    private ListView listaVendidos;
    private MaisVendidosAdapter adapter;

    private List<MaisVendidosObject> objectList;
    private List<BannerObject> bannerObjects;

    RecyclerView cardView;

    ProgressBar progressBanner, progressBanner2, progressBanner3;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressBanner = findViewById(R.id.progressBanner);
        progressBanner2 = findViewById(R.id.progressBanner2);
        progressBanner3 = findViewById(R.id.progressBanner3);
        viewPager = findViewById(R.id.viewPager);
        circleIndicator = findViewById(R.id.circleIndicator);
        listaVendidos = findViewById(R.id.listaVendidos);
        cardView= findViewById(R.id.cardView);


        /**
         * Inicializando API de conexao
         */
        AndroidNetworking.initialize(MainActivity.this);
        AndroidNetworking.setParserFactory(new JacksonParserFactory());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setCheckable(false);
        navigationView.getMenu().getItem(1).setCheckable(false);
        navigationView.getMenu().getItem(2).setCheckable(false);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_USE_LOGO);
        getSupportActionBar().setIcon(R.drawable.logo_menu);
        getSupportActionBar().setTitle("");

        /**
         * ADAPTER PARA POPUAR OS BANNERS
         */
        getBanner();

        /**
         * ADAPTER PARA POPULAR CATEGORIAS
         */
        FragmentManager fm = getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragmentContainer);

        if (fragment == null) {
            fragment = new HorizontalListViewFragment();
            fm.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }

        /**
         * ADAPTER PARA POPULAR MAIS VENDIDOS
         */
        getItensMaisVendidos();


        listaVendidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MaisVendidosObject object = (MaisVendidosObject) parent.getAdapter().getItem(position);
                Intent ir = new Intent(MainActivity.this, DescProdutosFloat.class);
                ir.putExtra(DATA_OBJECT_VENDIDOS, object);
                startActivity(ir);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.home_menu) {
            // Handle the camera action
        } else if (id == R.id.sobre_menu) {
            item.setChecked(false);
            startActivity(new Intent(MainActivity.this, Sobre.class));

        }else if(id == R.id.menu_info){
            startActivity(new Intent(MainActivity.this, InfoDev.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getItensMaisVendidos(){
        objectList = new ArrayList<>();
        objectList.clear();

        String url = URL_PADRAO+ROTA_PRODUTO_MAIS_VENDIDO;
        progressBanner2.setVisibility(View.VISIBLE);
        AndroidNetworking.get(url)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d("ON-RESPONSE", "============="+ jsonObject.toString());
                        progressBanner2.setVisibility(View.GONE);

                        try {
                                JSONArray array = (JSONArray) jsonObject.get("data");

                                for(int j=0; j<array.length(); j++){
                                    JSONObject json = (JSONObject) array.get(j);

                                    MaisVendidosObject object = new MaisVendidosObject(
                                            json.getInt(VENDIDO_ID),
                                            json.getString(VENDIDO_NOME),
                                            json.getString(VENDIDO_IMAGEM),
                                            json.getString(VENDIDO_DESCRICAO),
                                            json.getString(VENDIDO_PRECO_DE),
                                            json.getString(VENDIDO_PRECO_POR));

                                    objectList.add(object);
                                }
                            adapter = new MaisVendidosAdapter(MainActivity.this, objectList);
                            listaVendidos.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("ON-anError", "============="+ anError.toString());
                        progressBanner2.setVisibility(View.GONE);
                    }

                });
    }

    public void getBanner(){
        bannerObjects = new ArrayList<>();
        bannerObjects.clear();

        String url = URL_PADRAO+ROTA_BANNER;

        progressBanner.setVisibility(View.VISIBLE);
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d("ON-RESPONSE", "============="+ jsonObject.toString());

                        progressBanner.setVisibility(View.GONE);
                        try {
                            JSONArray array = (JSONArray) jsonObject.get("data");

                            for(int j=0; j<array.length(); j++){
                                JSONObject json = (JSONObject) array.get(j);

                                BannerObject object = new BannerObject(
                                        json.getInt(BANNER_ID),
                                        json.getString(BANNER_IMAGE),
                                        json.getString(BANNER_LINK));

                                bannerObjects.add(object);

                            }


                            viewPager.setAdapter(new SliderAdapter(MainActivity.this, bannerObjects));
                            circleIndicator.setViewPager(viewPager);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("ON-anError", "============="+ anError.toString());
                        progressBanner.setVisibility(View.GONE);
                    }

                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            getBanner();
            getItensMaisVendidos();

            FragmentManager fm = getSupportFragmentManager();

            if (fragment == null) {
                fm.beginTransaction()
                        .add(R.id.fragmentContainer, fragment)
                        .commit();
            }else{
                getSupportFragmentManager().beginTransaction().detach(fragment).attach(fragment).commit();
            }

        }
        return super.onOptionsItemSelected(item);
    }
}
