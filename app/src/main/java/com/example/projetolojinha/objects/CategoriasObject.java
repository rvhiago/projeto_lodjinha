package com.example.projetolojinha.objects;

import java.io.Serializable;

public class CategoriasObject implements Serializable {
    String nome;
    String image;
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public CategoriasObject(int id, String nome, String image) {
        this.nome = nome;
        this.image = image;
        this.id = id;
    }
}
