package com.example.projetolojinha.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.projetolojinha.objects.MaisVendidosObject;
import com.example.projetolojinha.R;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.List;

public class MaisVendidosAdapter extends BaseAdapter {
    List<MaisVendidosObject> mListVendidos;
    Context context;
    LayoutInflater mInflater;

    public MaisVendidosAdapter(Context context, List<MaisVendidosObject> mListVendidos){
        this.context = context;
        this.mListVendidos = mListVendidos;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return mListVendidos.size();
    }

    @Override
    public Object getItem(int position) {
        return mListVendidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MaisVendidosObject vendidosObject = mListVendidos.get(position);

        ViewHolder holder = null;

        if(convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_vendidos, null);

            holder.desc =  convertView.findViewById(R.id.descProduto);
            holder.precoAntes =  convertView.findViewById(R.id.valorAntes);
            holder.precoDepois =  convertView.findViewById(R.id.valorDepois);
            holder.image =  convertView.findViewById(R.id.imgProduto);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.desc.setText(vendidosObject.getNome());
        holder.precoAntes.setText("De: " + vendidosObject.getPrecoDe());
        holder.precoDepois.setText("Por: " +vendidosObject.getPrecoPor());


        RequestOptions options = new RequestOptions()
                .error(R.drawable.logo)
                .centerCrop()
                .priority(Priority.HIGH);

        Glide.with(context)
                .asBitmap()
                .apply(options)
                .load(vendidosObject.getUrlImagem())
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@android.support.annotation.Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        //progressPicture.setVisibility(View.GONE);
                        //Toast.makeText(context, "Erro ao carregar foto de perfil", Toast.LENGTH_SHORT).show();

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {


                        return false;
                    }
                })
                .into(holder.image);

      /*  Picasso.get()
                .load(vendidosObject.getUrlImagem())
                .resize(50, 50)
                .centerCrop()
                .error(R.drawable.logo)
                .into(holder.image);*/

        /**
         * Colocar risco no Text do preço (strikethroug)
         */
        holder.precoAntes.setPaintFlags(holder.precoAntes.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        return convertView;
    }

    static class ViewHolder{
        TextView desc;
        TextView precoAntes;
        TextView precoDepois;
        ImageView image;

    }
}
